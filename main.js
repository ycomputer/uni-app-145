// #ifndef VUE3
import Vue from 'vue'
import App from './App'
// 导入网络请求库
import {
	$http
} from '@escook/request-miniprogram'
// 导入 store 实例
import store from './store/store'

// 设置baseURL
// $http.baseUrl = 'https://api-hmugo-web.itheima.net/api/public/v1'
$http.baseUrl = 'https://uinav.com/api/public/v1'


// 请求拦截器
$http.beforeRequest = function(options) {
	// 弹出一个加载框
	uni.showLoading({
		title: '数据加载中...'
	})

	// 判断请求的是否为有权限的 API 接口
	if (options.url.indexOf('/my/') !== -1) {
		// 为请求头添加身份认证字段
		options.header = {
			// 字段的值可以直接从 vuex 中进行获取
			Authorization: store.state.m_user.token,
		}
	}
}
// 响应拦截器
$http.afterRequest = function() {
	// 隐藏数据加载框
	uni.hideLoading()
}
// 将$http挂载到uni全局对象
uni.$http = $http
// 网络请求库配置结束

// 配置全局消息提示框
uni.$showMsg = function(title = '数据加载失败', duration = 1500) {
	uni.showToast({
		title,
		duration,
		icon: 'none'
	})
}


Vue.config.productionTip = false

App.mpType = 'app'

// vue实例
const app = new Vue({
	...App,
	store
})
app.$mount()
// #endif











// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
import App from './App.vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
