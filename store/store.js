// 目标：创建一个store实例

// 导入vue和vuex
import Vue from 'vue'
import Vuex from 'vuex'

// 导入具体的业务模块
// 购物车
import moduleCart from './cart'
// 用户信息
import moduleUser from './user'

// 把vuex安装位vue的插件
Vue.use(Vuex)

// 创建 Store 实例对象
const store = new Vuex.Store({
	// 一个模块就是一个单独的文件
	modules: {
		'm_cart': moduleCart,
		'm_user': moduleUser
	}
})

export default store
