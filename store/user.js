// 用户模块，保存用户相关信息


export default {

	namespaced: true,

	state: () => ({
		// 用户地址信息
		// address: {}
		address: JSON.parse(uni.getStorageSync('address') || '{}'),
		// 用户登录后的token
		token: uni.getStorageSync('token') || '',
		// 微信用户信息
		userinfo: JSON.parse(uni.getStorageSync('userinfo') || '{}'),
		// 重定向的 object 对象 { openType, from }
		redirectInfo: null
	}),

	mutations: {
		// 保存收获地址信息
		updateAddress(state, address) {
			state.address = address

			// 保存到本地
			this.commit('m_user/saveAddressToStorage')
		},
		// 保存收获地址信息到本地
		saveAddressToStorage(state) {
			uni.setStorageSync('address', JSON.stringify(state.address))
		},

		// 保存微信用户信息
		saveUserInfo(state, userinfo) {
			state.userinfo = userinfo
			// 通过 this.commit() 方法，调用 m_user 模块下的 saveUserInfoToStorage 方法，将 userinfo 对象持久化存储到本地
			this.commit('m_user/saveUserInfoToStorage')
		},
		// 将 userinfo 持久化存储到本地
		saveUserInfoToStorage(state) {
			uni.setStorageSync('userinfo', JSON.stringify(state.userinfo))
		},

		// 保存token
		updateToken(state, token) {
			state.token = token
			this.commit('m_user/saveTokenToStorage')
		},
		// 将 token 持久化存储到本地
		saveTokenToStorage(state) {
			uni.setStorageSync('token', state.token)
		},

		// 更新重定向的信息对象
		updateRedirectInfo(state, info) {
			state.redirectInfo = info
		}


	},

	getters: {
		addstr(state) {
			// 收获地址的拼接
			if (JSON.stringify(state.address) === '{}') return ''

			// 拼接 省，市，区，详细地址 的字符串并返回给用户
			return state.address.provinceName + state.address.cityName + state.address.countyName + state.address
				.detailInfo
		}
	}

}
