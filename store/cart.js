// 这是 vuex 的一个模块 ===> state

// 要挂载到store模块属性中
export default {

	// 开启模块化
	namespaced: true,

	// 模块数据存储地：state，是一个对象
	state: () => {
		// 购物车数据，购物车存放多个商品
		// cart: [],
		// 注意key不存在时，居然返回 ''
		console.log('===', uni.getStorageSync('cart').length)
		// 从本地读取
		return {
			cart: JSON.parse(uni.getStorageSync('cart') || '[]')
		}
	},

	// 修改state数据的方法
	mutations: {
		// 多个改变 state-cart 数据的方法
		// 修改购物车数据，添加商品到购物车
		addToCart(state, goods) {
			// 修改购物车数据判断逻辑
			// 如果添加的是同样一件商品，只修改商品对象的数量
			// 去购物车数组中，找这个商品是否添加过
			console.log('before', JSON.stringify(state.cart))
			const result = state.cart.find((item) => item.goods_id === goods.goods_id)
			// 如果没有找到，直接添加到购物车列表
			if (!result) {
				console.log('没找到')
				// 添加到开头还是结尾，产品来定
				state.cart.push(goods)
			} else {
				// 找到，只修改商品的数量即可
				console.log('找到了')
				result.goods_count += 1
			}
			console.log('after', JSON.stringify(state.cart))
			// 保存购物车数据到本
			this.commit('m_cart/saveToStorage')

		},
		// 更新购物车商品的选中状态
		updateGoodsState(state, goods) {
			// 找到要修改的商品
			const result = state.cart.find((item) => item.goods_id === goods.goods_id)
			if (result) {
				result.goods_state = goods.goods_state
			}
			// 保存购物车数据到本地
			this.commit('m_cart/saveToStorage')
		},

		// 更新购物车商品的数量
		updateGoodsCount(state, goods) {
			// 找到要修改的商品
			const result = state.cart.find((item) => item.goods_id === goods.goods_id)
			if (result) {
				result.goods_count = goods.goods_count
			}
			// 保存购物车数据到本地
			this.commit('m_cart/saveToStorage')
		},

		// 从购物车中删除商品
		removeGoodsById(state, goods) {
			console.log('====', goods)
			// 过滤出未删除的商品
			const newCart = state.cart.filter(item => item.goods_id !== goods.goods_id)
			state.cart = newCart
			// 保存购物车数据到本地
			this.commit('m_cart/saveToStorage')
		},

		// 修改购物车商品选中状态
		updateAllGoodsState(xxx, newState) {
			xxx.cart.forEach(item => item.goods_state = newState)
			// 保存购物车数据到本地
			this.commit('m_cart/saveToStorage')

		},


		// 保存数据到本地
		saveToStorage(state) {
			uni.setStorageSync('cart', JSON.stringify(state.cart))
		}
	},

	// 获取state-cart的计算属性
	getters: {
		// 多个计算属性
		// 计算产品数量
		total(state) {
			// 计算数组中元素的 goods_count 的累加值。
			// let total = 0;
			// state.cart.forEach(item => {
			// 	total = total + item.goods_count
			// })
			// return total

			// 数组中的那个方法是累加操作
			const total = state.cart.reduce((acc, item) => acc + item.goods_count, 0)
			return total
		},

		// 要结算的商品数量
		checkedCount(state) {
			// 1 筛选出准备结算商品数组
			const arr1 = state.cart.filter(item => item.goods_state)
			// 2 累加计算商品总数
			const checkedCount = arr1.reduce((acc, item) => acc + item.goods_count, 0)
			return checkedCount
		},

		// 结算商品的总价格
		checkedGoodsAmount(state) {
			// 1 筛选出准备结算商品数组
			const arr1 = state.cart.filter(item => item.goods_state)
			// 2 累加计算商品总价格
			const price = arr1.reduce((acc, item) => acc + (item.goods_count * item.goods_price), 0)
			return price.toFixed(2)
		},

		// 购物车商品全选状态计算
		isFullCheck(state) {
			// 判断数组中的商品是否都是 goods_state === true
			const isFullCheck = state.cart.every(item => item.goods_state)
			return isFullCheck
		},

	}

}
